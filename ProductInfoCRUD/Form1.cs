﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using ExcelDataReader;
using System.IO;

namespace ProductInfoCRUD
{
    public partial class Form1 : Form
    {
        string connectionString = @"Server=127.0.0.1;Database=product_test;uid=appuser;pwd='emc2am';SslMode=none;";
        int productID = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            using (MySqlConnection mySqlConn = new MySqlConnection(connectionString))
            {
                mySqlConn.Open();
                MySqlCommand mySqlCmd = new MySqlCommand("ProductInfoAddOrEdit", mySqlConn);
                mySqlCmd.CommandType = CommandType.StoredProcedure;
                mySqlCmd.Parameters.AddWithValue("_ProductID", productID);
                mySqlCmd.Parameters.AddWithValue("_ProductName", txtProductName.Text.Trim());
                mySqlCmd.Parameters.AddWithValue("_Manufacturer", txtManufacturer.Text.Trim());
                mySqlCmd.Parameters.AddWithValue("_Qty", txtQuantity.Text.Trim()=="" ? 0 : Convert.ToInt32(txtQuantity.Text.Trim()));
                mySqlCmd.Parameters.AddWithValue("_Description", txtDescription.Text.Trim());
                mySqlCmd.Parameters.AddWithValue("_FullDescription", txtFullDescription.Text.Trim());
                mySqlCmd.Parameters.AddWithValue("_ProdCode", txtProdCode.Text.Trim());
                mySqlCmd.Parameters.AddWithValue("_Active", CheckBoxToInt());
                mySqlCmd.ExecuteNonQuery();
                MessageBox.Show("Submitted Successfully");
                Clear();
                GridFill();
                   
            }
        }

        private void GridFill()
        {
            using (MySqlConnection mySqlConn = new MySqlConnection(connectionString))
            {
                mySqlConn.Open();
                MySqlDataAdapter mySqlDa = new MySqlDataAdapter("ProductInfoViewAll", mySqlConn);
                mySqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
                DataTable dtblBook = new DataTable();
                mySqlDa.Fill(dtblBook);
                dgvProduct.DataSource = dtblBook;
                //dgvProduct.Columns[0].Visible = false;
            }
        }

        private void Clear()
        {
            txtProductName.Text = txtManufacturer.Text = txtQuantity.Text = txtProdCode.Text = txtDescription.Text = txtFullDescription.Text = txtSearch.Text = "";
            chkIsActive.Checked = false;
            productID = 0;
            btnSave.Text = "Save";
            btnDelete.Enabled = false;
        }

        private int CheckBoxToInt()
        {
            return chkIsActive.Checked ? 1 : 0;
        }

        private void btnViewAll_Click(object sender, EventArgs e)
        {
            Clear();
            GridFill();
        }

        private void dgvProduct_DoubleClick(object sender, EventArgs e)
        {
            if (dgvProduct.CurrentRow.Index != -1)
            {
                productID = Convert.ToInt32(dgvProduct.CurrentRow.Cells[0].Value.ToString());
                txtProductName.Text = dgvProduct.CurrentRow.Cells[1].Value.ToString();
                txtManufacturer.Text = dgvProduct.CurrentRow.Cells[2].Value.ToString();
                txtQuantity.Text = dgvProduct.CurrentRow.Cells[3].Value.ToString();
                txtDescription.Text = dgvProduct.CurrentRow.Cells[4].Value.ToString();
                txtFullDescription.Text = dgvProduct.CurrentRow.Cells[5].Value.ToString();
                chkIsActive.Checked = Convert.ToInt32(dgvProduct.CurrentRow.Cells[6].Value.ToString()) > 0 ? true : false;
                txtProdCode.Text = dgvProduct.CurrentRow.Cells[7].Value.ToString();
               
                btnSave.Text = "Update";
                btnDelete.Enabled = true;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            using (MySqlConnection mySqlConn = new MySqlConnection(connectionString))
            {
                mySqlConn.Open();
                MySqlCommand mySqlCmd = new MySqlCommand("ProductInfoDeleteByID", mySqlConn);
                mySqlCmd.CommandType = CommandType.StoredProcedure;
                mySqlCmd.Parameters.AddWithValue("_ProductID", productID);
                mySqlCmd.ExecuteNonQuery();
                MessageBox.Show("Deleted Successfully");
                Clear();
                GridFill();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Clear();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            using (MySqlConnection mySqlConn = new MySqlConnection(connectionString))
            {
                mySqlConn.Open();
                MySqlDataAdapter mySqlDa = new MySqlDataAdapter("ProductInfoSearchByValue", mySqlConn);
                mySqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
                mySqlDa.SelectCommand.Parameters.AddWithValue("_SearchValue", txtSearch.Text);
                DataTable dtblBook = new DataTable();
                mySqlDa.Fill(dtblBook);
                dgvProduct.DataSource = dtblBook;
                //dgvProduct.Columns[0].Visible = false;
            }
        }

        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            string insertCmd = "insert into product_info(ProductName, Manufacturer, Qty, Description, FullDescription, Active, ProdCode) values (@ProductName, @Manufacturer, @Qty, @Description, @FullDescription, @Active, @ProdCode)";

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";

            if (openFileDialog.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }

            FileStream stream = new FileStream(openFileDialog.FileName, FileMode.Open);
            IExcelDataReader excelDataReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            DataSet result = excelDataReader.AsDataSet();

            using (MySqlConnection mySqlConn = new MySqlConnection(connectionString))
            {
                mySqlConn.Open();

                MySqlTransaction transaction = mySqlConn.BeginTransaction();

                var mySqlDataAdapterSelect = new MySqlDataAdapter("select * from product_info", mySqlConn);
                var ds = new DataSet();
                mySqlDataAdapterSelect.Fill(ds, "product_info");

                var mySqlDataAdapter = new MySqlDataAdapter();

                mySqlDataAdapter.InsertCommand = new MySqlCommand(insertCmd, mySqlConn);

               
                mySqlDataAdapter.InsertCommand.Parameters.Add("@ProductName", MySqlDbType.VarChar, 250, "ProductName");
                mySqlDataAdapter.InsertCommand.Parameters.Add("@Manufacturer", MySqlDbType.VarChar, 45, "Manufacturer");
                mySqlDataAdapter.InsertCommand.Parameters.Add("@Qty", MySqlDbType.Int32, 11, "Qty");
                mySqlDataAdapter.InsertCommand.Parameters.Add("@Description", MySqlDbType.VarChar, 150, "Description");
                mySqlDataAdapter.InsertCommand.Parameters.Add("@FullDescription", MySqlDbType.VarChar, 350, "FullDescription");
                mySqlDataAdapter.InsertCommand.Parameters.Add("@Active", MySqlDbType.Byte, 4, "Active");
                mySqlDataAdapter.InsertCommand.Parameters.Add("@ProdCode", MySqlDbType.VarChar, 45, "ProdCode");
                mySqlDataAdapter.InsertCommand.UpdatedRowSource = UpdateRowSource.None;


                foreach (DataTable table in result.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        DataRow row = ds.Tables["product_info"].NewRow();
                        row["Qty"] = Convert.ToInt32(dr.Field<double>(2));
                        row["Active"] = Convert.ToByte(dr.Field<double>(3));
                        row["Manufacturer"] = dr.Field<String>(4);
                        row["Description"] = dr.Field<String>(5);
                        row["FullDescription"] = dr.Field<String>(6);
                        row["ProductName"] = dr.Field<String>(7);
                        row["ProdCode"] = dr.Field<String>(8);
                        ds.Tables["product_info"].Rows.Add(row);
                    }
                }

                mySqlDataAdapter.UpdateBatchSize = 100;
                mySqlDataAdapter.Update(ds, "product_info");

                transaction.Commit();

                Clear();
                GridFill();
            }

        }
    
    }
}
